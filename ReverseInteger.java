package com.indivara;

public class ReverseInteger {

	public static void main(String[] args) {
	      int i = 1234567, rev = 0;
	      System.out.println("Original: " + i);
	      while(i != 0) {
	         int digit = i % 10;
	         rev = rev * 10 + digit;
	         i /= 10;
	      }
	      System.out.println("Reversed: " + rev);
	   }
}
